<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index',function(){
    return view('trangchu');
});

Auth::routes();

Route::get('home',
    ["as" => "home", "uses" =>"HomeController@getindex"]);
Route::get('dangkychuxe', ['as' => 'dangkychuxe', 'uses' => 'UserController@create']);
Route::get('customer',['as'=>'customer', 'uses' => 'UserController@customer']);
Route::get('danhgia', ['as' => 'danhgia', 'uses' => 'ReviewController@index']);
Route::get('chitiethoadon', ['as'=>'chitiethoadon', 'uses' => 'UserController@chitiethoadon']);
Route::get('doanhthu', ['as' => 'doanhthu', 'uses' => 'UserController@showdoanhthu']);
Route::get('danhsachthuexe', ['as' => 'danhsachthuexe', 'uses' => 'OrderController@danhsachthuexe']);
Route::get('datxe', ['as' => 'datxe', 'uses' => 'OrderController@datxe']);
Route::get('khuyenmai', ['as' => 'khuyenmai', 'uses' => 'OrderDetailController@khuyenmai']);
Route::get('lich', ['as' => 'lich', 'uses' => 'OrderController@lich']);
Route::get('bieudo', ['as' => 'bieudo', 'uses' => 'UserController@bieudo']);
Route::get('xemchitiet',['as' => 'xemchitiet', 'uses' => 'MotorbikeController@getChitiet']);
//Route::get('/dangnhap' , 'HomeController@getLogin');
//Route::get('/dangky' ,'HomeController@getRegister');
Route::get('chitietsanpham/{id}' ,['as' => 'chitietsanpham', 'uses' => 'HomeController@chitietsanpham']);
Route::get('search', ['as'=>'search', 'uses' => 'HomeController@Search']);
Route::get('buy', ['as'=>'buy', 'uses' => 'OrderController@Buy']);



Route::post('dangky' ,['as' => 'dangky', 'uses' => 'HomeController@postRegister']);
Route::post('dangnhap' ,['as' => 'dangnhap', 'uses' => 'HomeController@postLogin']);

Route::get('/admin','AuthController@getLogin')->name('login');
Route::post('/admin','AuthController@postLogin')->name('clogin');
Route::group(['prefix'=>'admin','middleware'=>['checkLogin','checkRole']],function(){
    Route::get('/list','UserController@getLayout')->name('layout');
    Route::get('/listRoleOwner/{id}','UserController@getLayoutOwner')->name('layoutOwner');
    Route::get('/logout','AuthController@logout')->name('logout');
    Route::get("/owner","UserController@getAll");
    // edit user
    Route::get("/owner/edit/{id}","UserController@getEdit");
    // do edit
    Route::post("/owner/edit/{id}","UserController@postEdit")->name('postEdit');
    // ad user
    Route::get("/owner/add","UserController@getAdd");
    // do_addd
    Route::post("/owner/add","UserController@postAdd")->name('postAdd');
    // xoa
    Route::get("/owner/delete/{id}","UserController@delete");
    //list motorbike
    Route::get("/motorbike/list/{id}","MotorbikeController@getAll")->name('getList');
    //get add motorbike
    Route::get("/motorbike/add/{id}","MotorbikeController@getAdd");
    // do add
    Route::post("/motorbike/add","MotorbikeController@postAdd")->name('postAddMotorbike');
    //get edit motorbike
    Route::get("/motorbike/edit/{id}","MotorbikeController@getEdit");
    // post edit motorbike
    Route::post("/motorbike/edit/{id}","MotorbikeController@postEdit")->name('postEditMotorbike');
    // delete motorbike
    Route::get("motorbike/delete/{id}","MotorbikeController@delete");
    // user
    Route::get("/user","UserController@getUserAll");
    // edit user
    Route::get("/user/edit/{id}","UserController@getUserEdit");
    // do edit
    Route::post("/user/edit/{id}","UserController@postUserEdit")->name('postUserEdit');

    // ad user "UserController@postUserEdit")->name('postUserEdit');
    Route::get("/user/add","UserController@getUserAdd");
    // do_addd
    Route::post("/user/add","UserController@postUserAdd")->name('postUserAdd');
    // xoa
    Route::get("/user/delete/{id}","UserController@userdelete");

    // promotion
    //list motorbike
    Route::get("/promotion/list/{id}","PromotionController@getPromotionAll")->name('getPromotionList');
    //get add motorbike
    Route::get("/promotion/add/{id}","PromotionController@getPromotionAdd");
    // do add
    Route::post("/promotion/add","PromotionController@postPromotionAdd")->name('postAddPromotion');
    //get edit motorbike
    Route::get("/promotion/edit/{id}","PromotionController@getPromotionEdit");
    // post edit motorbike
    Route::post("/promotion/edit/{id}","PromotionController@postPromotionEdit")->name('postEditPromotion');
    // delete motorbike
    Route::get("promotion/delete/{id}","PromotionController@deletePromotion");

    Route::group(['prefix'=>'/owner'],function(){

        Route::get("/listMotobike/{id}","MotorbikeController@getListAll")->name('getListMotorbikeOfOwner');
        Route::get("/motorbike/add/{id}","MotorbikeController@getMotorbikeAdd");
        Route::post("motorbike/add","MotorbikeController@postMotorbikeAdd")->name('postAddMotorbikeOfOwner');
        Route::get("/motorbike/edit/{id}","MotorbikeController@getEditMotorbike");
        Route::post("/motorbike/edit/{id}","MotorbikeController@postEditMotorbike")->name('postEditMotorbikeOfOwner');
        Route::get("/motorbike/delete/{id}","MotorbikeController@deleteOfOwner");

        // promotion
        Route::get("/listPromotion/{id}","PromotionController@getListAll")->name('getListPromotionOfOwner');
        Route::get("/promotion/add/{id}","PromotionController@getPromotionAddOfOwner");
        Route::post("promotion/add","PromotionController@postPromotionAddOfOwner")->name('postAddPromotionOfOwner');
        Route::get("/promotion/edit/{id}","PromotionController@getEditPromotionOfOwner");
        Route::post("/promotion/edit/{id}","PromotionController@postPromotionEditOfOwner")->name('postEditPromotionOfOwner');
        Route::get("/promotion/delete/{id}","PromotionController@deleteOfOwnerPromotion");
        Route::get("/logout","AuthController@logout");

        // profile
        Route::get("/editProfile/{id}","UserController@getEditOfOwner");

        Route::post("/editProfile/{id}", "UserController@postEditOfOwner")->name('posteditProfile');

    });
});