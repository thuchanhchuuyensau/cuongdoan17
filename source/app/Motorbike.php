<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    protected $table='motorbikes';

    protected $fillable=['image','user_id','price_day','price_week','status','detail','required'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function images(){
        return $this->hasMany('App\Images');
    }
}
