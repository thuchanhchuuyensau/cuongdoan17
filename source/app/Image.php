<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table='images';

    protected $fillable=['name','motorbike_id'];

    public function motorbike(){

        return $this->belongsTo('App\Motorbike','motorbike_id');
    }
}
