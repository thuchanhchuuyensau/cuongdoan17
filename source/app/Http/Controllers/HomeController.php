<?php

namespace App\Http\Controllers;
use App\Motorbike;
use App\Review;
use App\User;
use App\Image;
use App\Promotion;
use Hash;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        return view('page.trangchu');
    }

    public function getindex()
    {
        $chuxe = DB::table('users')->where('role_id',2)->get()->take(5);
        $sale = DB::table('promotions')->select('value')->get();
        $chuxemoi = DB::table('users')->where('role_id',2)->orderBy('id','DESC')->get()->take(4);
        $sugnxe = DB::table('users')->where('role_id', 2)->get()->take(5);
        return view('page.trangchu', compact('chuxe','sale','chuxemoi','sugnxe'));
    }
    public function getOwner()
    {
        $nxechitiet = DB::table('users')->where('role_id',2)->get();
        return view('page.chitietnhaxe' ,['nxechitiet'=> $nxechitiet]);
    }
    public function chitietsanpham($id){
        $nxechitiet = DB::table('users')->where('role_id',2)->where('id',$id)->first();
        $dsachxe = DB::table('motorbikes')->where('user_id',$nxechitiet->id)->get();
        return view('page.chitietnhaxe', compact('nxechitiet', 'dsachxe'));
    }
//    public function getLogin(){
//        return view('page.dangnhap');
//    }

    public function Search(Request $request){
        $tukhoa = $request->tukhoa;
        $srchnxe = User::where('address','like','%'.$tukhoa.'%')->get();
        return view('page.search', compact('srchnxe','tukhoa'));
    }


    public function postLogin(Request $req){
        $this->validate($req,
            [
                'username' => 'required',
                'password' => 'required|min:6|max:50',
            ],
            [
                'uesername.required' => 'Vui lòng nhập tài khoản',
                'password.required' => 'Vui lòng nhập pass',
            ]);
    }
//    public function getRegister(){
//        return view('page.dangky');
//    }

    public function postRegister(Request $request){
        $this->validate($request,
            [
                'email' =>'required|email|unique:users,email',
                'password' => 'required|min:6|max:50',
                'phonenumber' => 'required',
                'repassword' =>'required|same:password',
                'username' => 'required|unique:users,name',
                'address' => 'required',
            ],
            [
                'email.required' => 'Vui lòng nhập email',
                'email.unique' => 'Email đã có được sử dụng',
                'email.email' => 'Bạn vui lòng nhập đúng định dạng',
                'password.required' =>'Bạn không thể để trống ô này',
                'password.min' => 'Mật khẩu yêu cầu đủ 6 ký tự',
                'username.unique' =>'Tên này đã được sử dụng',
                'username.required' =>'Bạn vui lòng điền đầy đủ thông tin',
                'repassword.same' => 'Xin vui lòng nhập lại',
                'address.required' => 'Xin vui lòng điền đầy đủ thông tin'
            ]
        );
        $user = new User();
        $user->username = $request -> username;
        $user->password = Hash::make($request -> password);
        $user->phone = $request -> phone;
        $user->address = $request -> address;
        $user->save();

//        return redirect('trangchu')->with('thanhcong','Tạo tài khoản thành công');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }
}
