<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests\LoginRequest;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function getLogin(){
    	return view('admin.login');
    }
    public function postLogin(LoginRequest $request){
    	$login=[
    			'email'=>$request->email,
    			'password'=>$request->password
    		];

    		if(Auth::attempt($login)){
                if(Auth::user()->role_id ==1 || Auth::user()->role_id==2){
    			
    			         return redirect()->route('layout');
    		  }
              
                  			                else {
                    return redirect()->route('home');           }
            } else {
                    return redirect()->route('login')->with(['messages'=>'Vui lòng kiểm tra lại tài khoản và mật khẩu']);
 
     }           
    }

    
    public function logout(){
        if(Auth::check()){
            Auth::logout();
        }
        return redirect()->route('login');
    }
}
