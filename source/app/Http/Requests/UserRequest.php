<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
                    'name'=>'required|unique:users,name|min:1|max:100',
                    'role_id'=>'required|max:3',
                    'phone'=>'required|min:10|unique:users,phone|max:12',
                    'email'=>'required|unique:users,email|min:10|max:100',
                    'password'=>'min:6|max:50',
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ];
    }
    public function messages(){
        return[
                        'name.required'=>'bạn chưa nhập tên nhà xe',
                        'name.unique'=>'tên bạn đã nhập bị trùng',
                        'name.min'=>'Độ dài tên nhỏ hơn 1 ký tự',
                        'name.max'=>'Độ dài tên lớn 100 ký tự',
                        'role_id.required'=>'bạn chưa nhập quyền chủ xe',
                        'role_id.max'=>'giá trị của chủ xe là 2,người dùng là 3',
                        'phone.required'=>'bạn chưa nhập số điện thoại',
                        'phone.min'=>'độ dài số diện thoại nhỏ hơn 10',
                        'phone.max'=>'độ dài số diện thoại lớn hơn 12',
                        'email.required'=>'bạn chưa nhập email',
                        'email.min'=>'Độ dài của email nhỏ hơn 10',
                        'email.max'=>'Độ dài của email lớn hơn 100',
                        'password.min'=>'độ dài của mật khẩu nhỏ hơn 6',
                        'password.max'=>'độ dài số mật khẩu lớn hơn 50',
                        'image.mimes'=>'sai định dạng ảnh',
                        'image.required'=>'bạn chưa up ảnh',
                        'image.max'=>'dung lượng của ảnh vượt quá 2048K'
        ];
    
    }
}
