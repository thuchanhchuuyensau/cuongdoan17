<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'email'=>'required|max:50|min:10',
            'password'=>'required|max:50|min:8'
                    ];
    }
    public function messages(){
        return[
            'email.required'=>'Vui lòng nhập email',
            'email.max'=>'Độ dài của email quá lớn , vui lòng nhập ít hơn 50 ký tự',
            'email.min'=>'Độ dài của email quá nhỏ , vui lòng nhập nhiều hơn 10 ký tự',
            'password.required'=>'vui lòng nhập password',
            'password.max'=>'Độ dài của password quá lớn , vui lòng nhập ít hơn 50 ký tự',
            'password.min'=>'Độ dài của password quá nhỏ , vui lòng nhập nhiều hơn 8 ký tự'
        ];
    
    }
}
