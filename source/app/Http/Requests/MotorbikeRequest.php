<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MotorbikeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
                    'price_day'=>'required',
                    'price_week'=>'required',
                    'detail'=>'required',
                    'required'=>'required',
                    'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ];
    }
    public function messages(){
        return[
                        'price_day.required'=>'bạn chưa nhập giá theo ngày ',
                        'price_week.required'=>'bạn chưa  nhập giá theo tuần',
                        'detail.required'=>'bạn chưa nhập thông tin về xe',
                        'required.required'=>'bạn chưa nhập yêu cầu của xe',
                        'image.mimes'=>'sai định dạng ảnh',
                        'image.max'=>'dung lượng của ảnh vượt quá 2048K'
        ];
    
    }
}
