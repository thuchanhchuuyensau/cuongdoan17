@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Add motorbike </div>
		<div class="panel-body">
			<!--  -->
			 @if ($errors->any())
                    <ul class="error-form">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
			<form method="post" action="{{ route('postAddMotorbike')}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
			<input type="hidden" name="user_id" value="{{$id}}">
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">price_day</div>
				<div class="col-md-9">
					<input type="text" name="price_day" class="form-control" placeholder="vui lòng nhập giá theo ngày">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">price_week</div>
				<div class="col-md-9">
					<input type="text" name="price_week" class="form-control" placeholder="vui lòng nhập giá theo tuần ">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">detail</div>
				<div class="col-md-9">
					<textarea name="detail" class="form-control" style="height:250px;">	
	
					</textarea>
					<script type="text/javascript">
					CKEDITOR.replace('detail');
					</script>
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">required</div>
				<div class="col-md-9">
					<input type="text" name="required" class="form-control" placeholder="vui lòng nhập yêu cầu">
				</div>
			</div>
			<!-- end row -->
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">status</div>
				<!-- <div class="col-md-9">
					<input type="text" name="status" class="form-control" placeholder="trạng thái nhập giá trị 0 hoặc 1 " >
				</div> -->
				<label class="col-md-3"><input type="radio" name="status" value="1">Visible</label>
				<label class="col-md-3"><input type="radio" name="status" value="0">Invisible</label>
			</div>
			<!-- end row -->
			<!-- row --
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">image</div>
				<div class="col-md-9">
					<input type="file" name="image">
				</div>
			</div>
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			
			</form>
		</div>
	</div>
</div>
@endsection