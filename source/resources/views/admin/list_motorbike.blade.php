@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-12 col-xs-offset-0">
	<div class="panel panel-primary">
		<div class="panel-heading">List Motorbikes</div>
		<div style="margin:15px 10px">
			<a href="{{url('/admin/motorbike/add/'.$id)}}" class="btn btn-primary">Add</a>
		</div>
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				
				<tr>
					<th style="width:20px;">STT</th>
					<th style="width: 30px;">price_day</th>
					<th style="width: 100px;">price_week</th>
					<th style="width: 50px;">image</th>
					<th style="width: 100px;">detail</th>
					<th style="width :30px;">required</th>
					<th style="width :30px;">status</th>
					<th style="width:200px;">options</th>
				</tr>
				<?php $stt = 0; ?>
				@foreach( $motorbikes as $motorbike)
				<?php $stt++; ?>
				<tr>
					<td>{{ $stt }}</td>
					<td>{{ $motorbike->price_day }}</td>
					<td>{{$motorbike->price_week}}</td>
					<td>
						@if(file_exists("upload/owner/motorbike/".$motorbike->image))
						<img style="width: 100px; height: 100px;" src="{{ asset('upload/owner/motorbike/'.$motorbike->image)}}">
						@endif
					</td>
					<td>{{$motorbike->detail}}</td>

					<td>{{ $motorbike->required}}</td>
					<td>{{ $motorbike->status}}</td>

					<td style="text-align:center">
						<a href="{{url('admin/motorbike/edit/'.$motorbike->id)}}">Edit</a>&nbsp;|&nbsp;
						<a href="{{url('admin/motorbike/delete/'.$motorbike->id)}}" onclick="return window.confirm('Are you sure?');">Delete</a>
					</td>
				</tr>
				@endforeach
			</table>
			<style type="text/css">
				.pagination{padding:0px; margin:0px;}			
			</style>
			
	</div>
</div>
@endsection