@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Edit Motorbike</div>
		<div class="panel-body">

			<form method="POST" action="{{route('postEditMotorbike',['id'=>$id])}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">price_day</div>
				<div class="col-md-9">
					<input type="text" name="price_day" class="form-control" value="{{ isset($motorbike->price_day) ?$motorbike->price_day:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">price_week</div>
				<div class="col-md-9">
					<input type="text" name="price_week" class="form-control" value="{{ isset($motorbike->price_week) ?$motorbike->price_week:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">detail</div>
				<div class="col-md-9">
					<textarea name="detail" class="form-control" style="height:250px;">	
					{{ $motorbike->detail }} 
					</textarea>
					<script type="text/javascript">
					CKEDITOR.replace('detail');
					</script>
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">required</div>
				<div class="col-md-9">
					<input type="text" name="required" class="form-control" value="{{ isset($motorbike->required) ?$motorbike->required:'' }}">
				</div>
			</div>
			<!-- end row -->
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">status</div>
				<div class="col-md-9">
					<input type="number" name="status" value="{{ $motorbike->status}}">
				</div>
			</div>
			
			<!-- end row -->			
				
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Ảnh</div>
				<div class="col-md-9">
					<input type="file" name="image">
				</div>
			</div>
			
			<!-- end row -->			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			<!-- end row -->
			</form>
		</div>
	</div>
</div>
@endsection