@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Edit Motorbike</div>
		<div class="panel-body">

			<form method="POST" action="{{route('postEditPromotion',['id'=>$id])}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Name</div>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" value="{{ isset($promotion->name) ?$promotion->name:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Value</div>
				<div class="col-md-9">
					<input type="text" name="value_p" class="form-control" value="{{ isset($promotion->value_p) ?$promotion->value_p:'' }}">
				</div>
			</div>
			<!-- end row -->
					
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			<!-- end row -->
			</form>
		</div>
	</div>
</div>
@endsection
