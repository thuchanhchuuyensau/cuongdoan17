@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-12 col-xs-offset-0">
	<div class="panel panel-primary">
		<div class="panel-heading">List Motorbikes</div>
		<div style="margin:15px 10px">
			<a href="{{url('/admin/promotion/add/'.$id)}}" class="btn btn-primary">Add</a>
		</div>
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				
				<tr>
					<th style="width:20px;">STT</th>
					<th style="width: 30px;">Name</th>
					<th style="width: 100px;">Value</th>
					<th style="width: 100px;">Action</th>
				</tr>
				<?php $stt = 0; ?>
				@foreach( $promotions as $promotion)
				<?php $stt++; ?>
				<tr>
					<td>{{ $stt }}</td>
					<td>{{ $promotion->name }}</td>
					<td>{{$promotion->value_p }}</td>
					
					<td style="text-align:center">
						<a href="{{url('admin/promotion/edit/'.$promotion->id)}}">Edit</a>&nbsp;|&nbsp;
						<a href="{{url('admin/promotion/delete/'.$promotion->id)}}" onclick="return window.confirm('Are you sure?');">Delete</a>
					</td>
				</tr>
				@endforeach
			</table>
			<style type="text/css">
				.pagination{padding:0px; margin:0px;}			
			</style>
			
	</div>
</div>
@endsection