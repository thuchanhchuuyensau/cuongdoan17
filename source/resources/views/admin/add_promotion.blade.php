@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Add Promotion</div>
		<div class="panel-body">
			<!--  -->
			 @if ($errors->any())
                    <ul class="error-form">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
			<form method="post" action="{{ route('postAddPromotion')}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
			<input type="hidden" name="user_id" value="{{$id}}">
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Name</div>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" placeholder="vui lòng nhập tên khuyến mại">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Value</div>
				<div class="col-md-9">
					<input type="text" name="value_p" class="form-control" placeholder="vui lòng nhập giá trị khuyến mại %">
				</div>
			</div>
			<!-- end row -->
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			
			</form>
		</div>
	</div>
</div>
@endsection