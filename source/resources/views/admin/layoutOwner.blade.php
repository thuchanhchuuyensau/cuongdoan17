<!DOCTYPE html>
<html>
<head>
	<title>Owner</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="{{ url('/backend/css/bootstrap.min.css') }}">
<!--	 load checkeditor-->
<script type="text/javascript" src="{{ url('/backend/ckeditor/ckeditor.js') }}"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">INNO VIHACLE</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">  
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li class="active"><a href="{{ route('getList',['id'=>$id])}}">List Motorbike</a></li>
            <li class="active"><a href="">Profile</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

   <div class="container" style="margin-top:70px;">
   	@yield("do-du-lieu")
   </div>

</body>
</html>