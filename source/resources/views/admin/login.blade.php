<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrap.min.css') }}">
</head>
<body>
<div class="container">
    <div class="row justify-content-center" style="margin-top: 15px;">
        <div class="col-md-6">
            <!-- card primary -->
             @if ($errors->any())
                    <ul class="error-form">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            <div class="card border-primary">
                @if(isset($messages)){
                <ul>
                @foreach($messages as $ms)
                <li>{{$ms}}</li>
                @endforeach
            </ul>
            @endif
            
                <div class="card card-header bg-primary text-white" style="padding:7px !important;">Login</div>
                <div class="card-body">
                    <!-- form -->
                    <form method="post" action="{{route('clogin')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- form group -->
                        <div class="form-group" style="padding-top: 15px;">
                            <div class="row">
                                <div class="col-md-3">Email</div>
                                <div class="col-md-9"><input type="email" name="email" required class="form-control"></div>
                            </div>
                        </div>
                        <!-- end form group -->
                        <!-- form group -->
                        <div class="form-group" style="padding-top: 0px;">
                            <div class="row">
                                <div class="col-md-3">Password</div>
                                <div class="col-md-9"><input type="password" name="password" required class="form-control"></div>
                            </div>
                        </div>
                        <!-- end form group -->
                        <!-- form group -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3"></div>

                                <div class="col-md-9">
                                    <input type="submit" value="Login" class="btn btn-primary"> <input type="Reset" value="Reset" class="btn btn-danger">
                                </div>
                            </div>
                        </div>
                        <!-- end form group -->
                    </form>
                    <!-- end form -->
                </div>
            </div>
            <!-- end card primary -->
        </div>
    </div>
</div>
</body>
</html>