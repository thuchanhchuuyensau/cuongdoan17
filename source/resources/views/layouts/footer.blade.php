<footer class="footer">
    <p class="f-slogan">Chúng tôi sẽ giúp bạn có chuyến đi rẻ nhất!</p>
    <div class="f-wrap">
        <ul class="f-nav">
            <li><a href="#">Về chúng tôi</a><span>|</span></li>
            <li><a href="#">Điều khoản</a><span>|</span></li>
            <li><a href="#">Chính sách bảo mật</a></li>
        </ul>
        <p class="f-copyright">Copyright &copy; 2019 <span>INNO VEH</span> </p>
    </div>
</footer>
<!-- backtotop -->
<a class="back-to-top" data-original-title="" title="" style="display: inline;"><i class="fa fa-angle-up"></i></a>
