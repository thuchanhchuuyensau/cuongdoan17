<div class="form-sign">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="">Đăng nhập</a></li>
        <li><a data-toggle="tab" href="">Đăng ký</a></li>
    </ul>

    <div class="tab-content">
        <div id="dangnhap" class="tab-pane fade in active">
            <h3>Vui lòng đăng nhập để tiếp tục</h3>
            <form action="{{URL::route('login')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                @if(Session::has('flag'))
                <div class="alert-alert-{{Session::get('flag')}}">{{Session::get('message')}}</div>
                @endif
                <div class="input-group">
                    <label>Tên đăng nhập</label>
                    <input type="text" name="tendangnhap">
                </div>
                <div class="input-group">
                    <label>Mật khẩu</label>
                    <input type="password" name="matkhau">
                </div>
                <div class="input-group">
                    <input type="submit" value="Đăng nhập">
                </div>
            </form>
            <div class="form-sign-social">
                <span><i class="fab fa-facebook-square"></i></span><span><i class="fab fa-google-plus-square"></i></span>
            </div>
            <div class="form-sign-forgot"><a href="">Quên mật khẩu</a></div>
        </div> <!-- dangnhap -->
        <div id="dangky" class="tab-pane fade">
            <h3>Vui lòng đăng ký để tiếp tục</h3>
            <form action="">
                <div class="input-group">
                    <label>Tên đăng nhập</label>
                    <input type="text" name="tendangky">
                </div>
                <div class="input-group">
                    <label>Mật khẩu</label>
                    <input type="password" name="matkhau">
                </div>
                <div class="input-group">
                    <label>Họ tên</label>
                    <input type="password" name="hoten">
                </div>
                <div class="input-group">
                    <label>Ngày sinh</label>
                    <input type="date" name="ngaysinh">
                </div>
                <div class="input-group">
                    <label>Giới tính</label>
                    <input type="checkbox" name="gioitinh-nu" value="Nữ">Nữ<input type="checkbox" name="gioitinh-nam" value="Nam">Nam
                </div>
                <div class="input-group">
                    <label>Số điện thoại</label>
                    <input type="tel" name="sodienthoai">
                </div>
                <div class="input-group">
                    <label>Địa chỉ</label>
                    <input type="text" name="diachi">
                </div>
                <div class="input-group">
                    <label>Email</label>
                    <input type="email" name="email">
                </div>
                <div class="input-group">
                    <input type="submit" value="Đăng ký">
                </div>
            </form>
            <div class="form-sign-social">
                <span><i class="fa fa-facebook-official" aria-hidden="true"></i></span><span><i class="fa fa-twitter" aria-hidden="true"></i></span>
            </div>
        </div><!--  dangki -->
    </div>
    <div class="form-sign-close"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
</div> <!-- sign -->