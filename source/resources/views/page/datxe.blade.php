<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Đặt xe</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">

    <!--Font-Awesome 4-->
    <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
    <!--owl-carousel-->
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
    <!--style-->
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>

<body>
<!--header-->
@include('layouts.header')

<!--g-nav-->
<nav class="g-nav">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link active" href="danhsachnhaxe.html">Thuê xe</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled c-main" href="#">Thuê cano</a>
        </li>
    </ul>
</nav>

<!--&lt;!&ndash;info-search&ndash;&gt;-->
<!--<section class="info">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="info__left col-6">-->
<!--                <p class="info__txt">Hà Nội</p>-->
<!--                <p class="info__txt">Xe ô tô - Tự lái</p>-->
<!--                <p class="info__txt">4 - 7 chỗ ngồi</p>-->
<!--            </div>-->

<!--            <div class="info__right col-6">-->
<!--                <label class="info__date">-->
<!--                    <input class="form-control" type="date">-->
<!--                </label>-->
<!--                <label class="info__date">-->
<!--                    <input class="form-control" type="date">-->
<!--                </label>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->



<!--dat xe-->
<section class="booked">
    <div class="container">
        <div class="booked__nav">
            <a href="danhsachnhaxe.html">Thuê xe</a>
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
            <a href="thongtinnhaxe.html">Thông tin nhà xe</a>
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
            <a href="thanhtoan.html">Thanh toán</a>
        </div>
        <div class="booked__content">
            <h3 class="text-center font-weight-bold pb-4">Thanh toán</h3>

            <div class="row">
                <p class="col-sm-6 font-weight-bold">Nhà xe: Nguyễn Văn Anh</p>
                <p class="col-sm-6">Mã đặt: <span class="font-weight-bold">9999999</span></p>
            </div>

            <table class="table">
                <thead class="table__head">
                <tr class="table__row">
                    <th class="table__cell table__cell--head" scope="col">Loại xe</th>
                    <th class="table__cell table__cell--head" scope="col">Số lượng</th>
                    <th class="table__cell table__cell--head" scope="col">Ngày đặt xe</th>
                    <th class="table__cell table__cell--head" scope="col">Ngày trả xe</th>
                    <th class="table__cell table__cell--head" scope="col">Địa chỉ nhận xe</th>
                    <th class="table__cell table__cell--head" scope="col">Giá</th>
                </tr>
                </thead>

                <tbody class="table__body">
                <tr class="table__row">
                    <th class="table__cell table__cell--head" scope="row" data-title="Loại xe">
                        7 chỗ
                    </th>
                    <td class="table__cell" data-title="Số lượng">
                        <span>1</span>
                    </td>
                    <td class="table__cell" data-title="Ngày đặt xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Ngày trả xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Địa chỉ nhận xe">
                        21 Nguyễn Văn Trỗi, Hà Đông, HN
                    </td>
                    <td class="table__cell" data-title="Giá">
                        2.500.000 VND
                    </td>
                </tr>
                <tr class="table__row">
                    <th class="table__cell table__cell--head" scope="row" data-title="Loại xe">
                        7 chỗ
                    </th>
                    <td class="table__cell" data-title="Số lượng">
                        <span>1</span>
                    </td>
                    <td class="table__cell" data-title="Ngày đặt xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Ngày trả xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Địa chỉ nhận xe">
                        21 Nguyễn Văn Trỗi, Hà Đông, HN
                    </td>
                    <td class="table__cell" data-title="Giá">
                        2.500.000 VND
                    </td>
                </tr>
                </tbody>
            </table>
            <p>Phụ phí</p>

            <table class="table table-sm table-bordered">
                <thead class="table__head">
                <tr class="table__row">
                    <th class="table__cell table__cell--head" scope="col">Loại phụ phí</th>
                    <th class="table__cell table__cell--head" scope="col">Số lượng</th>
                    <th class="table__cell table__cell--head" scope="col">Ngày đặt xe</th>
                    <th class="table__cell table__cell--head" scope="col">Ngày trả xe</th>
                    <th class="table__cell table__cell--head" scope="col">Giá</th>
                </tr>
                </thead>

                <tbody>
                <tr class="table__row">
                    <th class="table__cell table__cell--head" scope="row" data-title="Loại phụ phí">
                        Lều trại
                    </th>
                    <td class="table__cell" data-title="Số lượng">
                        <span>2</span>
                    </td>
                    <td class="table__cell" data-title="Ngày đặt xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Ngày trả xe">
                        <input class="border-0" type="date" value="2011-08-19">
                    </td>
                    <td class="table__cell" data-title="Giá">
                        1.500.000 VND
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="text-right">Tổng tiền:<span class="font-weight-bold"> 4.380.000 VND</span></div>

            <div class="form-group">
                <label for="comment">Ghi chú:</label>
                <textarea class="form-control" placeholder="Nhập văn bản" cols="100%" rows="2" id="comment"></textarea>
            </div>

            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="xacnhan" name="example1">
                <label class="custom-control-label" for="xacnhan">Tôi đồng ý với điều khoản chính sách của nhà xe</label>
            </div>
            <hr>
            <div class="form-group">
                <label for="comment">Chọn hình thức thanh toán để hoàn thành đơn đặt xe*</label>

            </div>
            <ul class="booked__content-thanhtoan">
                <li>
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span>Thanh toán khi nhận xe</span>
                </li>
                <li>
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                    <span>Thẻ tín dụng/Thẻ ghi nợ</span>
                </li>
                <li>
                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                    <span>Thẻ ATM nội địa</span>
                </li>

            </ul>
            <div class="form-group">
                <label for="comment">Thông tin bắt buộc*</label>

            </div>
            <div class="booked__btn">
                <button type="text" class="btn btn--main">Lưu</button>
            </div>
        </div>
    </div>
</section>

<!--footer-->
<footer class="f-sub">
    <ul class="f-sub-nav">
        <li><a href="#">Về chúng tôi</a><span>|</span></li>
        <li><a href="#">Điều khoản</a><span>|</span></li>
        <li><a href="#">Chính sách bảo mật</a></li>
    </ul>
    <p class="f-sub-copyright">Copyright &copy; 2019 <span>INNO VEH</span> </p>
</footer>

<div class="popup__thongbao">
    <p>Thông tin đặt xe của bạn đã được xác nhận</p>
    <button>OK</button>
</div> <!-- popup__thongbao -->

@include('layouts.lgvsrgt')

<!--footer-->
@include('layouts.footer')

<div class="popup__thongbao">
    <p>Thông tin đặt xe của bạn đã được xác nhận</p>
    <button>OK</button>
</div> <!-- popup__thongbao -->

<!-- jQuery library -->
<script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js')}}"></script>
<!-- Latest compiled JavaScript -->
<script src="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
<!--javascript-->
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>