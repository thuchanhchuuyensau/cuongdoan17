<!doctype html>
<html lang="vi">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<title>Trang cá nhân</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">
	<!--Font-Awesome-->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
	<!--style-->
	<link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>
<body>
<!--header-->
<header>
	<div class="container-fluid">
		<nav class="navbar navbar-expand-md">
			<!-- Brand -->
			<a class="navbar-brand" href="trangchu.html"><img class="h-logo" src="images/Logo.png" alt="Logo"></a>

			<!-- Toggler/collapsibe Button -->
			<button class="navbar-toggler ml-auto" type="button">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<!-- Navbar links -->
			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="h-link nav-link" href="#">Giới thiệu</a>
					</li>
					<li class="nav-item">
						<a class="h-email nav-link" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#"><img src="images/bn-vietnam.jpg" alt="VN"></a>
					</li>
					<li class="nav-item">
						<div class="h-user dropdown">
							<button class="h-user__btn dropdown-toggle" type="button" data-toggle="dropdown">
								<img class="h-user__avt" src="./images/avatar.JPG" alt="avatar">
								<span class="h-user__name">Trần Văn Nam</span>
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="{{URL::route('customer')}}">Trang cá nhân</a>
								<a class="dropdown-item" href="{{URL::route('dangkychuxe')}}">Làm nhà xe</a>
								<a class="dropdown-item" href="#">Đăng xuất</a>
							</div>
						</div>
					</li>
					<!-- sign -->
				</ul>
			</div>
		</nav>
	</div>
</header>

<!--Trang cá nhân khách hàng-->
<div class="customer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="customer__left">
					<img src="./images/avatar.JPG" alt="avatar">
					<p class="left-name">Trần Văn Nam</p>
					<div class="left-btns">
						<div class="left-btn">
							<button type="button" onclick="account()">
								<i id="icon-user" class="icon-btn fa fa-user" aria-hidden="true"></i>
								<span>Thông tin cá nhân</span>
								<i class="icon-arrow-right fa fa-angle-right" aria-hidden="true"></i>
							</button>
						</div>
						<div class="left-btn">
							<button type="button" onclick="lichsu()">
								<i id="icon-calendar" class="icon-btn fa fa-calendar" aria-hidden="true"></i>
								<span>Lịch sử</span>
								<i class="icon-arrow-right fa fa-angle-right" aria-hidden="true"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="customer__right">
					<div id="customer-info">
						<h6>Thông tin cá nhân</h6>
						<div class="account-list">
							<form action="#">
								<div class="form-group row">
									<label class="col-3 col-form-label">Họ tên </label>
									<input class="col-9 form-control" type="text" name="name-account">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Ngày sinh </label>
									<input class="col-9 form-control" type="date" name="name-account">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Giới tính </label>
									<input class="col-9 form-control" type="text" name="name-account">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Địa chỉ </label>
									<input class="col-9 form-control" type="text" name="address-user">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Số điện thoại </label>
									<input class="col-9 form-control" type="tel" name="name-user">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Email </label>
									<input class="col-9 form-control" type="email" name="name-user">
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Mật khẩu </label>
									<input class="col-9 form-control" type="pass" name="name-user">
									<i class="col-9 ml-auto mt-2 pl-0">Đổi mật khẩu</i>
								</div>

								<div class="btn-wrap">
									<input class="btn--radius" type="submit" value="Lưu thay đổi">
									<input class="btn--radius" type="submit" value="Hủy">
								</div>
							</form>
						</div>
					</div>
					<div id="customer-booked">
						<ul class="nav nav-tabs customer-booked-list">
							<li class="active"><a data-toggle="tab" href="#customer-booked-xe">Thuê xe</a></li>
							<li><a data-toggle="tab" href="#customer-booked-cano">Thuê cano</a></li>
							<li><a data-toggle="tab" href="#customer-booked-tonghop">Tổng hợp</a></li>
						</ul>
						<div class="tab-content">
							<div id="customer-booked-xe" class="tab-pane active">
								<div class="customer-booked-item">
									<p class="font-weight-bold">1. Ngày 12/3/2019</p>
									<div class="row">
										<p class="col-sm-6 font-weight-bold text-left">Nhà xe: Nguyễn Văn Anh</p>
										<p class="col-sm-6 text-right">Mã đặt: <span class="font-weight-bold">9999999</span></p>
									</div>

									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">Loại xe</td>
											<td scope="col">Số lượng</td>
											<td scope="col">Ngày nhận xe</td>
											<td scope="col">Ngày trả xe</td>
											<td scope="col">Địa điểm nhận xe</td>
											<td scope="col">Giá</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">Xe 7 chỗ</td>
											<td>1</td>
											<td>20/03/2018</td>
											<td>24/03/2018</td>
											<td>Tại nhà xe</td>
											<td>800.000 VND</td>
										</tr>
										<tr>
											<td scope="row">Xe 4 chỗ</td>
											<td>1</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>Tại nhà xe</td>
											<td>1.500.000 VND</td>
										</tr>
										</tbody>
									</table>
									<p class="font-weight-bold">Phụ phí</p>
									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">
												Loại phụ phí
											</td>
											<td scope="col">
												Số lượng
											</td>
											<td scope="col">
												Ngày nhận
											</td>
											<td scope="col">
												Ngày trả
											</td>
											<td scope="col">
												Giá
											</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">
												Lều trại
											</td>
											<td>
												<span>2</span>Số lượng
											</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>
												160.000 VND
											</td>
										</tr>
										</tbody>
									</table>
									<div class="row">
										<div class="booked-status col-6"><i>Trạng thái: Chờ nhận xe</i></div>
										<div class="text-right col-6">Tổng tiền:<span class="font-weight-bold"> 4.380.000 VND</span></div>
									</div>
									<div class="btn-xe">
											<span class="booked__btn">
												Hủy vé
											</span>
										<span>
												<a href="">Đánh giá</a>
											</span>
									</div>
								</div><!-- 1-booker -->
							</div>
							<div id="customer-booked-cano" class="tab-pane fade">
								<div class="customer-booked-item">
									<p class="font-weight-bold">1. Ngày 12/3/2019</p>
									<div class="row">
										<p class="col-sm-6 font-weight-bold text-left">Nhà xe: Nguyễn Văn Anh</p>
										<p class="col-sm-6 text-right">Mã đặt: <span class="font-weight-bold">9999999</span></p>
									</div>

									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">Loại xe</td>
											<td scope="col">Số lượng</td>
											<td scope="col">Ngày nhận xe</td>
											<td scope="col">Ngày trả xe</td>
											<td scope="col">Địa điểm nhận xe</td>
											<td scope="col">Giá</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">Xe 7 chỗ</td>
											<td>1</td>
											<td>20/03/2018</td>
											<td>24/03/2018</td>
											<td>Tại nhà xe</td>
											<td>800.000 VND</td>
										</tr>
										<tr>
											<td scope="row">Xe 4 chỗ</td>
											<td>1</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>Tại nhà xe</td>
											<td>1.500.000 VND</td>
										</tr>
										</tbody>
									</table>
									<p class="font-weight-bold">Phụ phí</p>
									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">
												Loại phụ phí
											</td>
											<td scope="col">
												Số lượng
											</td>
											<td scope="col">
												Ngày nhận
											</td>
											<td scope="col">
												Ngày trả
											</td>
											<td scope="col">
												Giá
											</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">
												Lều trại
											</td>
											<td>
												<span>2</span>Số lượng
											</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>
												160.000 VND
											</td>
										</tr>
										</tbody>
									</table>
									<div class="row">
										<div class="booked-status col-6"><i>Trạng thái: Chờ nhận xe</i></div>
										<div class="text-right col-6">Tổng tiền:<span class="font-weight-bold"> 4.380.000 VND</span></div>
									</div>
									<div class="btn-xe">
											<span class="booked__btn">
												Hủy vé
											</span>
										<span>
												<a href="">Đánh giá</a>
											</span>
									</div>
								</div><!-- 1-booker -->
							</div>
							<div id="customer-booked-tonghop" class="tab-pane fade">
								<div class="customer-booked-item">
									<p class="font-weight-bold">1. Ngày 12/3/2019</p>
									<div class="row">
										<p class="col-sm-6 font-weight-bold text-left">Nhà xe: Nguyễn Văn Anh</p>
										<p class="col-sm-6 text-right">Mã đặt: <span class="font-weight-bold">9999999</span></p>
									</div>

									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">Loại xe</td>
											<td scope="col">Số lượng</td>
											<td scope="col">Ngày nhận xe</td>
											<td scope="col">Ngày trả xe</td>
											<td scope="col">Địa điểm nhận xe</td>
											<td scope="col">Giá</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">Xe 7 chỗ</td>
											<td>1</td>
											<td>20/03/2018</td>
											<td>24/03/2018</td>
											<td>Tại nhà xe</td>
											<td>800.000 VND</td>
										</tr>
										<tr>
											<td scope="row">Xe 4 chỗ</td>
											<td>1</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>Tại nhà xe</td>
											<td>1.500.000 VND</td>
										</tr>
										</tbody>
									</table>
									<p class="font-weight-bold">Phụ phí</p>
									<table class="table table-sm table-bordered text-center">
										<thead>
										<tr>
											<td scope="col">
												Loại phụ phí
											</td>
											<td scope="col">
												Số lượng
											</td>
											<td scope="col">
												Ngày nhận
											</td>
											<td scope="col">
												Ngày trả
											</td>
											<td scope="col">
												Giá
											</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td scope="row">
												Lều trại
											</td>
											<td>
												<span>2</span>Số lượng
											</td>
											<td>20/05/2018</td>
											<td>24/05/2018</td>
											<td>
												160.000 VND
											</td>
										</tr>
										</tbody>
									</table>
									<div class="row">
										<div class="booked-status col-6"><i>Trạng thái: Chờ nhận xe</i></div>
										<div class="text-right col-6">Tổng tiền:<span class="font-weight-bold"> 4.380.000 VND</span></div>
									</div>
									<div class="btn-xe">
											<span class="booked__btn">
												Hủy vé
											</span>
										<span>
												<a href="">Đánh giá</a>
											</span>
									</div>
								</div><!-- 1-booker -->
							</div>

						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function account(){
		document.getElementById('customer-info').style.display="block";
		document.getElementById('customer-booked').style.display="none";

	}
	function lichsu(){
		document.getElementById('customer-info').style.display="none";
		document.getElementById('customer-booked').style.display="block";
	}
</script>

<!--footer-->
@include('layouts.footer')

<!-- jQuery library -->
<script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js')}}"></script>
<!-- Latest compiled JavaScript -->
<script src="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
<!--javascript-->
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>

