<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Lịch</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">
    <!--Front-Awesome-->
    <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
    <!--style-->
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>
<body>
<!--header-->
@include('layouts.header')

<nav class="owner-nav">
   <div class="container">
       <!-- Links -->
       <ul class="owner-nav__list">
           <li class="owner-nav__item">
               <a class="owner-nav__link active" href="{{URL::route('danhsachthuexe')}}">
                   <i class="fa fa-list" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Danh sách thuê xe</span>
               </a>
               <!--<span class="owner-nav__line">|</span>-->
           </li>
           <li class="owner-nav__item">
               <a class="owner-nav__link" href="{{URL::route('lich')}}">
                   <i class="fa fa-tag" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Lịch</span>
               </a>
               <!--<span class="owner-nav__line">|</span>-->
           </li>
           <li class="owner-nav__item">
               <a class="owner-nav__link" href="{{URL::route('danhgia')}}">
                   <i class="fa fa-tag" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Đánh giá </span>
               </a>
               <!--<span class="owner-nav__line">|</span>-->
           </li>
           <li class="owner-nav__item">
               <a class="owner-nav__link" href="{{URL::route('bieudo')}}">
                   <i class="fa fa-tag" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Biểu đồ</span>
               </a>
               <!--<span class="owner-nav__line">|</span>-->
           </li>
           <li class="owner-nav__item">
               <a class="owner-nav__link" href="{{URL::route('khuyenmai')}}">
                   <i class="fa fa-tag" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Khuyến mãi</span>
               </a>
               <!--<span class="owner-nav__line">|</span>-->
           </li>
           <li class="owner-nav__item">
               <a class="owner-nav__link" href="{{URL::route('nhaxe')}}">
                   <i class="fa fa-tag" aria-hidden="true"></i>
                   <span class="owner-nav__txt">Thông tin nhà xe</span>
               </a>
           </li>
       </ul>
   </div>
</nav>

<!--calendar-->
<section class="calendar">
    <div class="container">
       <div class="row">
           <div class="col-md-4">
               <input type="date">
           </div>
           <div class="col-md-8">
               <h6>Danh sách ngày 9 thg 3, 2019</h6>
               <!-- Tables begin -->
               <table class="table">
                   <thead class="table__head">
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="col">Tên khách hàng</th>
                       <th class="table__cell table__cell--head" scope="col">Ngày nhận - Ngày trả</th>
                       <th class="table__cell table__cell--head" scope="col">Mã đặt</th>
                       <th class="table__cell table__cell--head" scope="col">Loại xe</th>
                       <th class="table__cell table__cell--head" scope="col"></th>
                   </tr>
                   </thead>
                   <tbody class="table__body">

                   <tr class="table__row">
                       <th class="table__cell table__cell--head table__cell--spanned" colspan="5">
                           Ngày đặt: <span>14/03/2019</span>
                       </th>
                   </tr>

                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>


                   <tr class="table__row">
                       <th class="table__cell table__cell--head table__cell--spanned" colspan="5">
                           Ngày đặt: <span>13/03/2019</span>
                       </th>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>
                   <tr class="table__row">
                       <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                           Trần Thị An
                       </th>
                       <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                           <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                       </td>
                       <td class="table__cell" data-title="Mã đặt">
                           #12345
                       </td>
                       <td class="table__cell" data-title="Loại xe">
                           Xe 4 chỗ
                       </td>
                       <td class="table__cell">
                           <a class="btn-detail" href="{{URL::route('chitiethoadon')}}">Xem chi tiết</a>
                       </td>
                   </tr>


                   </tbody>
               </table>
               <!--end-table-->
           </div>
       </div>
    </div>
</section>
<!--footer-->
@include('layouts.footer')
</body>
</html>